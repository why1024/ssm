import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class TestUrl {
    public static void main(String[] args){
        // Ticket 获取 URL
        String ticketUrl = "http://localhost:8080/corpservice/authorize/ticket";
        String appKey="***";//携程提供
        String appSecurity="***";//携程提供
        //Json 串
        String ticketPostString = "{\"appKey\":\""+appKey+"\",\"appSecurity\":\""+appSecurity+"\"}";
        //POST
        String ticketResponse = sendPost(ticketUrl, ticketPostString);
        //构造 JSON 对象,需要导入 net.sf.json 包
        JSONObject jsonObject = (JSONObject)JSONSerializer.toJSON(ticketResponse);
        //获取 Ticket
        String ticket=(String)jsonObject.get("Token");
    }
    public static String sendPost(String url, String param) {
        OutputStreamWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            HttpURLConnection conn = null;
            conn = (HttpURLConnection) realUrl.openConnection();// 打开和 URL 之间的连接
            // 发送 POST 请求必须设置如下两行
            conn.setRequestMethod("POST"); // POST 方法
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.connect();
            out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");// 获取 URLConnection 对象对应的输出流
            out.write(param);// 发送请求参数
            System.out.println(param);
            out.flush();// flush 输出流的缓冲
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));//定义 BufferedReader 输入流来读取 URL的响应
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
                result += line;
                System.out.println("OK");
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }
        //使用 finally 块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }
}
