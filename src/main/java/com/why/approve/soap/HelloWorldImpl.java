package com.why.approve.soap;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(endpointInterface = "com.why.approve.soap.HelloWorld")////这里指定服务的接口类的路径，也可以不写
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class HelloWorldImpl implements HelloWorld {
    public String sayHi(String text)
    {
        System.out.println("sayHi called");
        return "Hello " + text;
    }
}