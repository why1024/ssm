package com.why.approve.soap;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.BindingType;

@WebService(targetNamespace="com.why.approve.soap")//如果不写targetNamespace的值时，默认是包反转，比如服务器项目中包是com.gstd.hw，那么默认值为hw.gstd.com，如果在另外
//的项目客户端中调用，则创建接口类HelloWorld时，类名可以不一样，但是targetNamespace必须一样。不然调用不成功！最好自己定义一个名称
@SOAPBinding(style = SOAPBinding.Style.RPC)
@BindingType(value =javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
public interface HelloWorld {
    String sayHi(@WebParam(name = "text") String text);
}
