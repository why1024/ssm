package com.why.approve.param;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ToString
@Getter
@Setter
public class TicketModel {
    private String appKey;
    private String appSecurity;
    private Integer tokenType;
    private Integer groupId;
    private Float version;

}
