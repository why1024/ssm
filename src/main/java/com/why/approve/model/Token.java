package com.why.approve.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.util.JSONPObject;

@Getter
@Setter
@Builder
public class Token {
    private  Integer code;//0:成功   1 参数验证失败  2：接口异常
    private String message;
    private Boolean success;
    private String Token;
}
