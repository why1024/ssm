package com.why.approve.common.exception;

import lombok.Getter;

@Getter
public enum BizErrorEnum {
    SERVER_ERROR(10301000,"服务器未知错误，请联系管理员。"),
    ;

    private int errorCode;
    private String errorMessage;

    private BizErrorEnum(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
