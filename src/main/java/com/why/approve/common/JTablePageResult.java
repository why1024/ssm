package com.why.approve.common;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

//import org.codehaus.jackson.annotate.JsonProperty;

@Getter
@Setter
@Builder
public class JTablePageResult<T>  implements Serializable {
    private static final long serialVersionUID = -895599442916010326L;

    @JsonProperty(value = "sEcho")
    private  Integer sEcho;
    @JsonProperty(value="iTotalRecords")
    private  Integer iTotalRecords;
    @JsonProperty(value="iTotalDisplayRecords")
    private  Integer iTotalDisplayRecords;
    private List<T> data = Lists.newArrayList();
}
