package com.why.approve.common;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;

public class PageQuery {

    @Getter
    @Setter
    @Min(value = 0, message = "当前页码不合法")
    private int start = 0;

    @Getter
    @Setter
    @Min(value = 1, message = "每页展示数量不合法")
    private int length = 10;


}
