package com.why.approve.controller;

import com.why.approve.model.Token;
import com.why.approve.param.TicketModel;
import com.why.approve.util.JsonMapper;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value="corpservice")
public class TicketController {

    @ResponseBody
    @RequestMapping(value = "authorize/ticket")
    public String createTicket(@RequestBody String ticket){
        Token d= Token.builder().code(2).build();
        log.info("enetr");
        TicketModel ticketModel= JsonMapper.string2Obj(ticket, new TypeReference<TicketModel>() {});
        log.info("ticketModel:{}",ticketModel);

        return "{Token:\"1234567890\"}";
    }
}
