package com.why.approve.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DPlayerController {

    @RequestMapping(value = "toIndex")
    public ModelAndView toMovieList(){
        return new ModelAndView("index");
    }

    @RequestMapping(value = "toPlayer")
    public ModelAndView toPlayer(){
        return new ModelAndView("play");
    }

    @RequestMapping(value = "toTest")
    public ModelAndView toTest(){
        return new ModelAndView("test");
    }

}
