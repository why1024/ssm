<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>播放页</title>
    <%@include file="/WEB-INF/views/layout/common.jsp"%>
</head>
<body>
<div id="dplayer"></div>
</body>
 <script>
     var dp = new DPlayer({
         container: document.getElementById('dplayer'),
         screenshot: false,
         video: {
             url: 'assets/mp4/demo.mp4',
             pic: 'assets/image/danmu.jpg',
         }
     });
 </script>
</html>
