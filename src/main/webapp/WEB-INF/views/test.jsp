<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/1/22
  Time: 22:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Disgusting Otaku TV</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dplayer/1.21.0/DPlayer.min.css">
</head>
<body>
<div id="dplayer"></div>
<script src="https://cdn.jsdelivr.net/npm/stats.js "></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js "></script>
<script src="https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js "></script>
<script src="https://cdn.jsdelivr.net/npm/flv.js/dist/flv.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/hls.js/dist/hls.min.js "></script>
<script src="https://cdn.jsdelivr.net/npm/dashjs/dist/dash.all.min.js "></script>
<script src="https://cdn.jsdelivr.net/webtorrent/latest/webtorrent.min.js "></script>
<script src="https://cdn.jsdelivr.net/npm/dplayer/dist/DPlayer.min.js"></script>
<script type="text/javascript">
    var dp = new DPlayer({
        container: document.getElementById('dplayer'),
        live: true,
        danmaku: true,
        apiBackend: {
            read: function (endpoint, callback) {
                console.log('假装 WebSocket 连接成功');
                callback();
            },
            send: function (endpoint, danmakuData, callback) {
                console.log('假装通过 WebSocket 发送数据', danmakuData);
                callback();
            }
        },
        video: {
            url: 'http://80.87.198.206/myapp/tvch/index.m3u8',
            type: 'hls'
        }
    });
</script>

</body>
</html>
